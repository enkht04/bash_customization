## Custom functions
function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "	 extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
    return 1
 else
    for n in $@
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)	 unlzma ./"$n"      ;;
            *.bz2)	 bunzip2 ./"$n"     ;;
            *.rar)	 unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.zip)	 unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)	 cabextract ./"$n"  ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}

function sshcp {
host=${2}
what=${1}
dest=${3}

if [ -z "${what}" -a "${host}" ]; then

        cat "${what}"| ssh "${host}" "cat > "${dest}/${1}" "
else
        echo -en "Sorry either file or host are empty \n"
fi
}

function taroveradb {
what=${1}
outfile=${2}
if [ -z "${what}" -a "${outfile}" ];then
        adb exec-out 'tar -cf - "${what}" 2>/dev/null' | pv > "${outfile}".tar
else "Sorry Parameter are empty"
fi
}

is_turbo_on() {
status="$(cat /sys/devices/system/cpu/intel_pstate/no_turbo)"
if [ "$status" -eq 1 ]; then
        echo "disabled 👎";
else
    	echo "enabled 👍";
fi
}

## kde disamble compositing
function comps_toggle {
	function status { qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.active; }
	function resume { qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.resume; }
	function suspend { qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.suspend; }
if [ $(status) = true ]; then (suspend);else (resume);fi
}

##  PulseAudio cycle through ouput sinks
function pa_toggleOUT {
IFS=$'\n' read -r -d '' -a sinks < <( pactl list sinks short|awk '{print $1}' && printf '\0' )
## how many sinks, adjusting starting from 0 run_num
sinks_amount="$((${#sinks[@]}-1))"
if [[ "$run_num" -le "$sinks_amount" ]];then
	#get value stored at $i position
	cur_sink="${sinks[$run_num]}"
	pacmd set-default-sink "$cur_sink"
	run_num=$((run_num+1))
else
	#reset run_num
	run_num=0
fi
}
export HISTTIMEFORMAT="%d/%m/%Y %T "
alias date='date +%Y-%m-%d:%H:%M:%S'
## GNU utils common
alias l="ls -lah --color=auto"
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias df='df -h'

## typos
alias mdkir="mkdir"
alias nestat="netstat"
## IP
alias filter_ips='grep -Po "\b(?:\d{1,3}\.){3}\d{1,3}\b"'
alias myextip="curl eth0.me"
alias myip='curl http://ipecho.net/plain; echo'
alias wanip4='dig @resolver1.opendns.com A myip.opendns.com +short -4'
alias ips='ifconfig -a | grep -o '\''inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)'\'' | awk '\''{ sub(/inet6? (addr:)? ?/, ""); print }'\'''
alias dnfup='sudo dnf update -y && sudo dnf upgrade -y'
alias rpm-listbySize="rpm -qa --queryformat '%10{size} - %-25{name} \t %{version} \t %{os} \n' | sort -rh | head -25 | awk '{print \$1/1024/1024, \$2, \$3, \$4}'"
alias virsh="export EDITOR=geany; virsh"
## CPU
alias core_pairing="cat /sys/devices/system/cpu/cpu*/topology/thread_siblings_list | sort | uniq"
alias curr_cpu_governor="sudo cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"
alias reload_udev="sudo udevadm control --reload-rules && sudo udevadm trigger"
alias ducks="du -cksh *| sort -rn | head"
alias share_http="ip ro;python3 -m http.server"
alias aria2c="aria2c -m 0 --continue=true"
alias rsync="rsync -avP"
alias decodeurl="~/bin/URLDefenseDecode.py ${1}"
## KDE
alias k5restart="kquitapp5 plasmashell || killall plasmashell && kstart5 plasmashell &>/dev/null & disown %%"
## Android
alias scrcpy_start="scrcpy -b2M -m800"
