#!/bin/bash
IFalias="if [ -f ~/.bash_aliases ];then source ~/.bash_aliases;fi"
URL="https://git.io/J3c8B"
Rtheme="arrow"
Utheme="simple"

function install_rbow {
curl -L "${URL}"| bash
	if [ $? -ne 0 ] ;then
		echo -e "\n Rainbow bash install failed"
	else 
		echo -e "\n Rainbow bash install succeeded"
	fi
}

function append {
echo "${1}" >> "${2}"
}

if [ "$EUID" -eq 0 ];then
	echo -e "\n Running as ROOT"
	install_rbow &&
	cat root_bashrc >> /root/.bashrc &&
	sed -i "s/rbw_load_theme simple/rbw_load_theme "$Rtheme"/g" /root/.bashrc &&
	append "$IFalias" ~/.bashrc &&
	cp bash_aliases ~/.bash_aliases

else
	install_rbow &&
	cat user_bashrc >> ~/.bashrc &&
	sed -i "s/rbw_load_theme arrow/rbw_load_theme "$Utheme"/g" ~/.bashrc &&
	append "$IFalias" ~/.bashrc &&
	cp bash_aliases ~/.bash_aliases
fi
